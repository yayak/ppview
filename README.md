# ppview

PowerPro LIVE Connection Monitor

*Featuring lightweight view of PowerPro client connection to its Firebird database and kill any connection at the meantime.*


###### Copyright 2015-2018 Yayak Zakaria (IT at Taman Dayu Golf Club & Resort, Pandaan Pasuruan)
> This code cannot be redistributed without permission from [Yayak Zakaria](mailto:muh.zakaria@gmail.com)

> More info just [contact us](mailto:muh.zakaria@gmail.com)!